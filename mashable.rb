require_relative 'standard'
require_relative 'mainclass'
require 'rest-client'
require 'json'
require 'colorize'
system("clear")

class Mashable < MainClass

	include KeyStandard

	  def initialize
	    @url = 'http://mashable.com/stories.json'
	  end

	  #Genera las noticias de Mashable
	  def getNews

			@num = 0
		  @link = []
		  puts "*********************************************** MASHABLE *********************************************************"
		  hash = JSON.parse(getJSON) 
		  hash["new"].each do |n|
				@date = DateTime.parse(n["post_date"])
				puts
			  puts "Título:".colorize(:color => :light_blue, :background => :red)+" #{n["title"]}"
			  puts "Autor:".colorize(:blue)+" #{n["author"]}"
			  puts "Fecha:".colorize(:green)+" #{Time.at(@date.to_time.to_i).strftime("%d/%m/%Y")}"
			  puts "Link:".colorize(:yellow)+" #{@num}) #{n["link"]}"
			  puts
			  @link[@num] = n["link"]
				@num += 1
				if @num % 5 == 0
					puts "Presione Enter para continuar"
					gets.chomp
				end
	  	end

			return @link
			openlink
	  	
	end
		  
end