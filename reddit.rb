require_relative 'standard'
require_relative 'mainclass'
require 'rest-client'
require 'json'
require 'colorize'
system("clear")

class Reddit < MainClass

	include KeyStandard

	  def initialize
	    @url = 'https://www.reddit.com/.json'
	  end

	  #Genera las noticias de Reddit
	  def getNews
		  
		  @num = 0
		  @link = []
		  puts "************************************************ REDDIT ************************************************************"
			hash = JSON.parse(getJSON)
			hash["data"]["children"].each do |n|
				@date = n["data"]["created"]
				puts
				puts "Título:".colorize(:color => :light_blue, :background => :red)+" #{n["data"]["title"]}"
			  puts "Autor:".colorize(:blue)+" #{n["data"]["author"]}"
			  puts "Fecha:".colorize(:green)+" #{Time.at(@date).strftime("%d/%m/%Y")}"
			  puts "Link:".colorize(:yellow)+" #{@num}) https://www.reddit.com#{n["data"]["permalink"]}"
			  puts
			  @link[@num] = "https://www.reddit.com"+n["data"]["permalink"]
				@num += 1
				if @num % 5 == 0
					puts "Presione Enter para continuar"
					gets.chomp
				end	
			end

			return @link
			openlink
			
		end

end