require 'rest-client'

module KeyStandard

	# Permite leer por teclado la opción que marque el usuario sin necesidad de presionar Enter
	def read_char
	  begin
	    # save previous state of stty
	    old_state = `stty -g`
	    # disable echoing and enable raw (not having to press enter)
	    system "stty raw -echo"
	    c = STDIN.getc.chr
	    # gather next two characters of special keys
	    if(c=="\e")
	      extra_thread = Thread.new{
	        c = c + STDIN.getc.chr
	        c = c + STDIN.getc.chr
	      }
	      # wait just long enough for special keys to get swallowed
	      extra_thread.join(0.00001)
	      # kill thread so not-so-long special keys don't wait on getc
	      extra_thread.kill
	    end
	  rescue => ex
	    puts "#{ex.class}: #{ex.message}"
	    puts ex.backtrace
	  ensure
	    # restore previous state of stty
	    system "stty #{old_state}"
	  end
	  return c
	end

end