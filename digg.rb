require_relative 'standard'
require_relative 'mainclass'
require 'rest-client'
require 'json'
require 'colorize'
system("clear")

class Digg < MainClass

	include KeyStandard

	  def initialize
	    @url = 'http://digg.com/api/news/popular.json'
	  end

	  #Genera las noticias de Digg
	  def getNews
		  
		  @num = 0
		  @link = []
			puts "************************************************ DIGG ************************************************************"
			hash = JSON.parse(getJSON)
			hash["data"]["feed"].each do |n|
				@date = n["date"]
				puts
				puts "Título:".colorize(:color => :light_blue, :background => :red)+" #{n["content"]["title_alt"]}"
			  puts "Autor:".colorize(:blue)+" #{n["content"]["author"]}"
			  puts "Fecha:".colorize(:green)+" #{Time.at(@date).strftime("%d/%m/%Y")}"
			  puts "Link:".colorize(:yellow)+" #{@num}) #{n["content"]["original_url"]}"
				puts
				@link[@num] = n["content"]["original_url"]
				@num += 1
				if @num % 5 == 0
					puts "Presione Enter para continuar"
					gets.chomp
				end	
			end
			
			return @link
			openlink

		end
		  
end