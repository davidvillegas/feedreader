require_relative 'reddit'
require_relative 'mashable'
require_relative 'digg'
require_relative 'showall'
require 'rest-client'
require 'json'
require 'colorize'

class MainMenu
	
	include KeyStandard

	#Método que permite selccionar las noticias que desea leer el usuario 
	def menu

		show_menu
		@opc = read_char.to_i

		loop	do
				
				if (@opc > 4)
					puts "\nPor Favor Seleccione una opción entre 0 y 4"
					sleep (1)
					menu
				else	
					case @opc
						when 1
							puts "\nCargando Noticias, espere un momento..."
							sleep(2)
							system ('clear')
							reddit = Reddit.new
							red = reddit.getNews
							reddit.openlink(red)
						when 2
							puts "\nCargando Noticias, espere un momento..."
							sleep(2)
							system ('clear')
							mashable = Mashable.new
							mash = mashable.getNews
							mashable.openlink(mash)
						when 3
							puts "\nCargando Noticias, espere un momento..."
							sleep(2)
							system ('clear')
							digg = Digg.new
							dig = digg.getNews
							digg.openlink(dig)
						when 4
							puts "\nCargando Noticias, espere un momento..."
							sleep(2)
							system ('clear')
							showall = ShowAll.new
							showall.show_array														
						when 0
							puts "\n¡Muchas Gracias por su Visita!"
							puts "Saliendo del Sistema..."
							sleep(2)
							system ('exit')
							system ('clear')	
					end
				end	
				break if (@opc<5)
		end		
		return @opc
	
	end

	#Muestra el Menú Principal
	def show_menu

			system('clear')
			puts "Bienvenidos al Sistema de Noticias en tiempo Real".blue.on_yellow.blink
			puts
			puts "1 - Leer Noticias de Reddit"
			puts
			puts "2 - Leer Noticias de Mashable"
			puts
			puts "3 - Leer Noticias de Digg"
			puts
			puts "4 - Leer Todas las Noticias"
			puts
			puts "0 - Salir del Sistema".colorize(:yellow)
			puts
			print "Seleccione su opción: ".blue.on_yellow.blink
		
	end

end	

mostrar = MainMenu.new
mostrar.menu