require_relative 'standard'
require_relative 'mainclass'
require 'rest-client'
require 'json'
require 'colorize'
system("clear")

class ShowAll < MainClass

	include KeyStandard

	  def initialize
			@urldigg = 'http://digg.com/api/news/popular.json'
			@urlreddit = 'https://www.reddit.com/.json'
			@urlmash = 'http://mashable.com/stories.json'
			@notices = []
			@noticesd = []
			@noticesr = []
			@noticesm = []
	  end

	  def getreddit
	    @response = RestClient.get(@urlreddit)
	    @response.body
	  end

	  def getdigg
	    @response = RestClient.get(@urldigg)
	    @response.body
	  end

	  def getmash
	    @response = RestClient.get(@urlmash)
	    @response.body
	  end

	  #Genera las noticias de Digg
	  def fill_board_digg

	  	hash = JSON.parse(getdigg)
	  		#i = 0
				hash["data"]["feed"].each_with_index do |n, i|
					@title = n["content"]["title"]
					@author = n["content"]["author"]
					@date = Time.at(n["date"]).strftime("%d-%m-%Y")
					@link = n["content"]["original_url"]
		    	@noticesd[i]=[]
		      @noticesd[i][0]=@title
		      @noticesd[i][1]=@author
		      @noticesd[i][2]=@date
		      @noticesd[i][3]=@link
	      end
	      return @noticesd
    end

    #Genera las noticias de Reddit
	  def fill_board_reddit
	  	hash = JSON.parse(getreddit)
	  		#i = 0
				hash["data"]["children"].each_with_index do |n, i|
					@title = n["data"]["title"]
					@author = n["data"]["author"]
					@date = Time.at(n["data"]["created"]).strftime("%d-%m-%Y")
					@link = "https://www.reddit.com"+n["data"]["permalink"]
		    	@noticesr[i]=[]
		      @noticesr[i][0]=@title
		      @noticesr[i][1]=@author
		      @noticesr[i][2]=@date
		      @noticesr[i][3]=@link
	      end
	      return @noticesr
    end 

    #Genera las noticias de Mashable
	  def fill_board_mashable
	  	hash = JSON.parse(getmash)
	  		#i = 0
				hash["new"].each_with_index do |n, i|
					@title = n["title"]
					@author = n["author"]
					@dat = DateTime.parse(n["post_date"])
					@date = Time.at(@dat.to_time.to_i).strftime("%d/%m/%Y")
					@link = n["link"]
		    	@noticesm[i]=[]
		      @noticesm[i][0]=@title
		      @noticesm[i][1]=@author
		      @noticesm[i][2]=@date
		      @noticesm[i][3]=@link
	      end
	      return @noticesm
    end

    #Método que guarda en un arreglo las noticias generadas por cada fuente y las ordena por fecha
    def fill_board

    	@noticesr = fill_board_reddit
    	@noticesm = fill_board_mashable
			@noticesd = fill_board_digg

    	@noticesr.each_with_index do |n,i|
    	 	@notices << @noticesr[i]
    	end 

    	@noticesm.each_with_index do |n,i|
    		@notices << @noticesm[i]
    	end
    		
    	@noticesd.each_with_index do |n,i| 
    		@notices << @noticesd[i]
    	end	
 			
 			@notices.sort_by! { |item| item[2] }.reverse
    	
    	return @notices

    end	

    #Muestra el arreglo con las noticias ordenadas
    def show_array

    	 @notices = fill_board
    	 
    	 @notices.each_with_index do |n,i|
    	 		puts "***************************************************************************************"
    	 		puts
	    	 	puts "Título: ".colorize(:color => :light_blue, :background => :red)+ "#{@notices[i][0]}"
	    	 	puts "Autor: ".colorize(:blue) + "#{@notices[i][1]}"
	    	 	puts "Fecha: ".colorize(:green) + "#{@notices[i][2]}"
	    	 	puts "Link: ".colorize(:yellow) + "#{@notices[i][3]}"
	    	 	puts
    		end
    		comeback	
    	 #puts @notices

    end

end