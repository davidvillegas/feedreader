require 'launchy'

class MainClass

		attr_accessor :url, :response, :link

	  def getinfo
	    @response = RestClient.get(@url)
	    @response.body
	  end

	  def getJSON
	    getinfo()
	  end

	  def comeback
	  	puts "¿ Desea Regresar al Menú Principal ?"
	  	puts "1 - Si"
	  	puts "0 - No (Salir)"
	  	@opc = read_char.to_i
	  	loop do
	  		if (@opc > 1)
	  			puts "\nOpción Inválida"
	  			comeback
	  		else	
		  		case @opc
			  		when 1
			  			show_menu = MainMenu.new
			  			show_menu.menu
			  		when 0
			  			puts "¡¡¡ Vuelva Pronto !!!"
			  			system('exit')
			  	end
			 end	
			break if (@opc<2)
			end
		end

		def openlink (link)
			
			print "Ingrese el Número (Link) de la Noticia a Leer y presione Enter: "
			@op = gets.chomp.to_i
			#if @op > (@link[@op].size)
			#	puts "Opción Mayor a la cantidad de links disponibles"
			#else
				Launchy.open("#{@link[@op]}")
			#end	
			comeback

		end	

end	